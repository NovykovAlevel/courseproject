package ua.igornovykov.domain.entity;

import java.util.Date;
import java.util.Objects;

public class Note {
    private String id;
    private String noteTitle;
    private String noteDescription;
    private NotePriority notePriority;
    private Date dateOfCreation;
    private Date dateOfExpiration;
    private NoteStatus noteStatus;

    public Note(String id ,String noteTitle, String noteDescription, NotePriority notePriority,
                Date dateOfCreation, Date dateOfExpiration) {
        this.id = id;
        this.noteTitle = noteTitle;
        this.noteDescription = noteDescription;
        this.notePriority = notePriority;
        this.dateOfCreation = dateOfCreation;
        this.dateOfExpiration = dateOfExpiration;
    }

    public NoteStatus getNoteStatus() {
        noteStatus = new Date().before(dateOfExpiration) ? NoteStatus.VALID : NoteStatus.INVALID;
        return noteStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Note)) return false;
        Note note = (Note) o;
        return getId().equals(note.getId()) &&
                Objects.equals(getNoteTitle(), note.getNoteTitle()) &&
                Objects.equals(getNoteDescription(), note.getNoteDescription()) &&
                getNotePriority() == note.getNotePriority() &&
                Objects.equals(getDateOfCreation(), note.getDateOfCreation()) &&
                Objects.equals(getDateOfExpiration(), note.getDateOfExpiration()) &&
                getNoteStatus() == note.getNoteStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getNoteTitle(), getNoteDescription(),
                getNotePriority(), getDateOfCreation(), getDateOfExpiration(),
                getNoteStatus());
    }

    public String getId() {
        return id;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public NotePriority getNotePriority() {
        return notePriority;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public Date getDateOfExpiration() {
        return dateOfExpiration;
    }

    public enum  NotePriority {
        HIGH,
        MIDDLE,
        LOW
    }

    public enum NoteStatus{
        VALID,
        INVALID
    }
}
