package ua.igornovykov.domain.use_case;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.domain.entity.Note;

public interface IUseCase {
    Completable saveNote(Note note);

    Completable removeNote(String noteId);

    Single<Note> getNote(String noteId);

    Single<List<Note>> getNoteList();
}
