package ua.igornovykov.domain.use_case.interactor;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.repository.INoteRepository;
import ua.igornovykov.domain.use_case.IUseCase;

public class NoteListInteractor implements IUseCase {
    private INoteRepository noteRepository;
    @Inject
    public NoteListInteractor(INoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public Completable saveNote(Note note) {
        return noteRepository.saveNote(note);
    }

    @Override
    public Completable removeNote(String noteId) {
        return noteRepository.removeNote(noteId);
    }

    @Override
    public Single<Note> getNote(String noteId) {
        return noteRepository.getNote(noteId);
    }

    @Override
    public Single<List<Note>> getNoteList() {
        return noteRepository.getAllNotes();
    }
}
