package ua.igornovykov.domain.repository;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.domain.entity.Note;

public interface INoteRepository {
    Completable saveNote (Note note);

    Completable removeNote(String noteId);

    Single<List<Note>> getAllNotes();

    Single<Note> getNote(String noteId);
}
