package ua.igornovykov.data_sqlite.repository.local_database.migration;

import android.database.sqlite.SQLiteDatabase;

import javax.inject.Inject;

import ua.igornovykov.data_sqlite.repository.local_database.IMigration;

public class NotesTableMigration implements IMigration {
    @Inject
    NotesTableMigration() {
    }

    @Override
    public int getTargetVersion() {
        return 1;
    }

    @Override
    public void execute(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE notes (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "title TEXT, " +
                "description TEXT, " +
                "priority INTEGER, " +
                "date_of_creation INTEGER, " +
                "date_of_expiration INTEGER)");
    }
}
