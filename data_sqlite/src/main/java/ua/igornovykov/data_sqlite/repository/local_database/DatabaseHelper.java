package ua.igornovykov.data_sqlite.repository.local_database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Set;

import javax.inject.Inject;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "notes.sql3";
    private static final int DATABASE_VERSION = 1;

    private Set<IMigration> migrations;


    @Inject
    DatabaseHelper(Context context, Set<IMigration> migrations) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.migrations = migrations;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (IMigration migration : migrations) {
            migration.execute(sqLiteDatabase);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        for (IMigration migration : migrations) {
            if (migration.getTargetVersion() > oldVersion
                    && migration.getTargetVersion() <= newVersion) {
                migration.execute(sqLiteDatabase);
            }
        }
    }
}
