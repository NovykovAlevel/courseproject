package ua.igornovykov.data_sqlite.repository.local_database;

import android.database.sqlite.SQLiteDatabase;

public interface IMigration {
    int getTargetVersion();

    void execute(SQLiteDatabase sqLiteDatabase);
}
