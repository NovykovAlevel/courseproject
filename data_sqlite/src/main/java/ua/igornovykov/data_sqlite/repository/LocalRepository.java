package ua.igornovykov.data_sqlite.repository;

import android.text.TextUtils;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data_sqlite.repository.mapper.NoteMapper;
import ua.igornovykov.data_sqlite.repository.local_database.DataBaseNoteRepository;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.repository.INoteRepository;

public class LocalRepository implements INoteRepository {
    private DataBaseNoteRepository dbNoteRepository;

    @Inject
    LocalRepository(DataBaseNoteRepository dbNoteRepository) {
        this.dbNoteRepository = dbNoteRepository;
    }

    @Override
    public Completable saveNote(Note note) {
        return Single.just(note)
                .map(NoteMapper::transform)
                .flatMapCompletable(
                        noteEntity -> (TextUtils.isEmpty(noteEntity.getNoteId()))
                                ? dbNoteRepository.add(noteEntity) : dbNoteRepository.update(noteEntity));
    }

    @Override
    public Completable removeNote(String noteId) {
        return Single.just(noteId)
                .flatMapCompletable(dbNoteRepository::remove);
    }

    @Override
    public Single<List<Note>> getAllNotes() {
        return dbNoteRepository.getAllNotes()
                .map(NoteMapper::transformList);
    }

    @Override
    public Single<Note> getNote(String noteId) {
        return Single.just(noteId)
                .flatMap(dbNoteRepository::getNote)
                .map(NoteMapper::transform);
    }
}
