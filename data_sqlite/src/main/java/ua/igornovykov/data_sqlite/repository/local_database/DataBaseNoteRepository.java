package ua.igornovykov.data_sqlite.repository.local_database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data_sqlite.repository.local_database.entity.NoteEntity;

public class DataBaseNoteRepository {
    private final String TABLE_NOTES = "notes";
    private final String COLUMN_ID = "id";
    private final String COLUMN_TITLE = "title";
    private final String COLUMN_DESCRIPTION = "description";
    private final String COLUMN_PRIORITY = "priority";
    private final String COLUMN_DATE_OF_CREATION = "date_of_creation";
    private final String COLUMN_DATE_OF_EXPIRATION = "date_of_expiration";

    private DatabaseHelper helper;

    @Inject
    DataBaseNoteRepository(DatabaseHelper helper) {
        this.helper = helper;
    }


    public Completable add(NoteEntity note) {
        return Completable.fromAction(() -> addNewNote(note));
    }

    public Completable update(NoteEntity note) {
        return Completable.fromAction(() -> updateNote(note));
    }

    public Single<List<NoteEntity>> getAllNotes() {
        return Single.fromCallable(this::getListNotes);
    }

    public Single<NoteEntity> getNote(String noteId) {
        return Single.fromCallable(() -> getNoteById(noteId));
    }

    public Completable remove(String noteId) {
        return Completable.fromAction(() -> removeNote(noteId));
    }

    private void addNewNote(NoteEntity noteEntity) {
        SQLiteDatabase db = helper.getWritableDatabase();
        db.insert(TABLE_NOTES, null, getContentValues(noteEntity));
        db.close();
    }

    private void updateNote(NoteEntity noteEntity) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{noteEntity.getNoteId()};

        SQLiteDatabase db = helper.getWritableDatabase();
        db.update(TABLE_NOTES, getContentValues(noteEntity), whereClause, whereArgs);
        db.close();
    }

    private List<NoteEntity> getListNotes() {
        String query = "SELECT * FROM " + TABLE_NOTES;

        List<NoteEntity> notes = new LinkedList<>();
        SQLiteDatabase db = helper.getReadableDatabase();

        try (Cursor cursor = db.rawQuery(query, null)) {
            if (cursor.moveToFirst()) {
                do {
                    notes.add(getNoteFromCursor(cursor));
                } while (cursor.moveToNext());
            }
        }
        db.close();
        return notes;
    }

    private NoteEntity getNoteById(String noteId) {
        NoteEntity noteEntity = null;
        String query = "SELECT * FROM " + TABLE_NOTES + " WHERE " + COLUMN_ID + " =?";
        String[] args = new String[]{noteId};

        SQLiteDatabase db = helper.getReadableDatabase();
        try (Cursor cursor = db.rawQuery(query, args)) {
            if (cursor.moveToFirst()) {
                noteEntity = getNoteFromCursor(cursor);
            }
        }
        db.close();
        return noteEntity;
    }

    private void removeNote(String noteId) {
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = new String[]{noteId};

        SQLiteDatabase db = helper.getWritableDatabase();
        db.delete(TABLE_NOTES, whereClause, whereArgs);
        db.close();
    }

    private NoteEntity getNoteFromCursor(Cursor cursor) {
        return new NoteEntity(
                cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(COLUMN_TITLE)),
                cursor.getString(cursor.getColumnIndex(COLUMN_DESCRIPTION)),
                cursor.getInt(cursor.getColumnIndex(COLUMN_PRIORITY)),
                cursor.getLong(cursor.getColumnIndex(COLUMN_DATE_OF_CREATION)),
                cursor.getLong(cursor.getColumnIndex(COLUMN_DATE_OF_EXPIRATION)));
    }

    private ContentValues getContentValues(NoteEntity note) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, note.getNoteTitle());
        values.put(COLUMN_DESCRIPTION, note.getNoteDescription());
        values.put(COLUMN_PRIORITY, note.getNotePriority());
        values.put(COLUMN_DATE_OF_CREATION, note.getDateOfCreation());
        values.put(COLUMN_DATE_OF_EXPIRATION, note.getDateOfExpiration());
        return values;
    }
}
