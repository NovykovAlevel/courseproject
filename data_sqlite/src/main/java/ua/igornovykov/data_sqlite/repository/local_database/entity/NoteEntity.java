package ua.igornovykov.data_sqlite.repository.local_database.entity;


public class NoteEntity {

    private String noteId;
    private String noteTitle;
    private String noteDescription;
    private int notePriority;
    private long dateOfCreation;
    private long dateOfExpiration;

    public NoteEntity(String noteId, String noteTitle, String noteDescription, int notePriority,
                      long dateOfCreation, long dateOfExpiration ) {
        this.noteId = noteId;
        this.noteTitle = noteTitle;
        this.noteDescription = noteDescription;
        this.notePriority = notePriority;
        this.dateOfCreation = dateOfCreation;
        this.dateOfExpiration = dateOfExpiration;
    }

    public String getNoteId() {
        return noteId;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public int getNotePriority() {
        return notePriority;
    }

    public long getDateOfCreation() {
        return dateOfCreation;
    }

    public long getDateOfExpiration() {
        return dateOfExpiration;
    }
}
