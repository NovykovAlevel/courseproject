package ua.igornovykov.data_firebase.repository.mapper;

import android.text.TextUtils;

import com.google.firebase.database.DataSnapshot;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import ua.igornovykov.data_firebase.repository.entities.NoteEntity;
import ua.igornovykov.domain.entity.Note;

public class NoteMapper {

    public static Map<String, Object> transformToMap(Note note) {
        Map<String, Object> noteMap = new HashMap<>();

        if (TextUtils.isEmpty(note.getId())) {
            String uuid = UUID.randomUUID().toString();
            noteMap.put("id", uuid);
        } else {
            noteMap.put("id", note.getId());
        }

        noteMap.put("title", note.getNoteTitle());
        noteMap.put("description", note.getNoteDescription());
        noteMap.put("priority", note.getNotePriority());
        noteMap.put("dateOfCreation", note.getDateOfCreation());
        noteMap.put("dateOfExpiration", note.getDateOfExpiration());
        return noteMap;
    }

    public static Note transformToNote(NoteEntity noteEntity) {
        return new Note(
                noteEntity.getNoteId(),
                noteEntity.getNoteTitle(),
                noteEntity.getNoteDescription(),
                noteEntity.getNotePriority(),
                noteEntity.getDateOfCreation(),
                noteEntity.getDateOfExpiration());
    }

    public static List<Note> noteEntityListToNoteList(List<NoteEntity> notes) {
        List<Note> result = new ArrayList<>(notes.size());
        for (NoteEntity noteEntity : notes) {
            result.add(transformToNote(noteEntity));
        }
        return result;
    }

    public static List<NoteEntity> dataSnapshotToNoteEntityList(DataSnapshot dataSnapshot) {
        List<NoteEntity> list = new ArrayList<>();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            list.add(snapshot.getValue(NoteEntity.class));
        }
        return list;
    }

    public static Note dataSnapshotToNote(DataSnapshot dataSnapshot) {
        NoteEntity noteEntity = dataSnapshot.getValue(NoteEntity.class);
        return transformToNote(noteEntity);
    }
}
