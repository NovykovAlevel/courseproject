package ua.igornovykov.data_firebase.repository;



import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data_firebase.repository.database.RealtimeDatabase;
import ua.igornovykov.data_firebase.repository.mapper.NoteMapper;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.repository.INoteRepository;

public class FirebaseRepository implements INoteRepository {
    private RealtimeDatabase database;

    @Inject
    public FirebaseRepository(RealtimeDatabase database) {
        this.database = database;
    }

    @Override
    public Completable saveNote(Note note) {
        return Single.just(note)
                .map(NoteMapper::transformToMap)
                .flatMapCompletable(database::add);
    }

    @Override
    public Completable removeNote(String noteId) {
        return database.remove(noteId);
    }

    @Override
    public Single<List<Note>> getAllNotes() {
        return database.getNotes()
                .map(NoteMapper::dataSnapshotToNoteEntityList)
                .map(NoteMapper::noteEntityListToNoteList);

    }

    @Override
    public Single<Note> getNote(String noteId) {
        return Single.just(noteId)
                .flatMap(database::getNote)
                .map(NoteMapper::dataSnapshotToNote);
    }
}