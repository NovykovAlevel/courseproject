package ua.igornovykov.data_firebase.repository.entities;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.Date;

import ua.igornovykov.domain.entity.Note;

@IgnoreExtraProperties
public class NoteEntity {
    private String noteId;
    private String noteTitle;
    private String noteDescription;
    private Note.NotePriority notePriority;
    private Date dateOfCreation;
    private Date dateOfExpiration;

    public NoteEntity() {
    }

    public NoteEntity(String noteId, String noteTitle, String noteDescription, Note.NotePriority notePriority, Date dateOfCreation,
                      Date dateOfExpiration) {
        this.noteId = noteId;
        this.noteTitle = noteTitle;
        this.noteDescription = noteDescription;
        this.notePriority = notePriority;
        this.dateOfCreation = dateOfCreation;
        this.dateOfExpiration = dateOfExpiration;
    }

    public String getNoteTitle() {
        return noteTitle;
    }

    public String getNoteDescription() {
        return noteDescription;
    }

    public Note.NotePriority getNotePriority() {
        return notePriority;
    }

    public String getNoteId() {
        return noteId;
    }

    public void setNoteId(String noteId) {
        this.noteId = noteId;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public Date getDateOfExpiration() {
        return dateOfExpiration;
    }
}
