package ua.igornovykov.data_firebase.repository.database;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;
import ua.igornovykov.data_firebase.repository.utils.RxFirebase;

public class RealtimeDatabase {

    private static final String PATH_TO_NOTE = "/notes/%s/notes/%s";
    private static final String PATH_TO_NOTES = "/notes/%s/notes";
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseDatabase database;


    @Inject
    public RealtimeDatabase() {
        database = FirebaseDatabase.getInstance();
    }

    public Completable add(Map<String, Object> noteMap) {
        return Completable.fromAction(() -> addNote(noteMap));
    }

    public Completable remove(String noteId) {
        return Completable.fromAction(() -> removeNoteById(noteId));
    }

    public Single<DataSnapshot> getNotes() {
        return RxFirebase.singleValueEventObserver(
                database.getReference(String.format(PATH_TO_NOTES, user.getUid())));
    }

    public Single<DataSnapshot> getNote(String noteId) {
        return RxFirebase.singleValueEventObserver(
                database.getReference(String.format(PATH_TO_NOTE, user.getUid(), noteId)));
    }

    private void addNote(Map<String, Object> noteMap) {
        Map<String, Object> result = new HashMap<>();
        result.put(String.format(PATH_TO_NOTE, user.getUid(), noteMap.get("id")), noteMap);

        database.getReference().updateChildren(result);
    }

    private void removeNoteById(String noteId) {
        database.getReference(String.format(PATH_TO_NOTE, user.getUid(), noteId))
                .removeValue();
    }
}
