package ua.igornovykov.todolist;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;


public class Application extends DaggerApplication {
    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().context(this).build();
    }

}
