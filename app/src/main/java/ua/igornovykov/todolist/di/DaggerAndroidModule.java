package ua.igornovykov.todolist.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import ua.igornovykov.todolist.ui.activity.note_editor.NoteEditActivity;
import ua.igornovykov.todolist.ui.activity.note_list.NoteListActivity;

@Module(includes = AndroidSupportInjectionModule.class)
interface DaggerAndroidModule {
    @ContributesAndroidInjector
    NoteListActivity mainActivity();

    @ContributesAndroidInjector(modules = {NoteEditModule.class})
    NoteEditActivity noteEditActivity();

    @ContributesAndroidInjector()
    LoginActivity loginActivity();

}
