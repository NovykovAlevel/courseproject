package ua.igornovykov.todolist.di;

import dagger.Provides;
import ua.igornovykov.todolist.ui.activity.note_editor.NoteEditActivity;

public class NoteEditModule {
    @Provides
    public String provideNoteId(NoteEditActivity activity) {
        return activity.getNoteId();
    }
}
