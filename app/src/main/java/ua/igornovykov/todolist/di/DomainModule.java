package ua.igornovykov.todolist.di;

import dagger.Binds;
import dagger.Module;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.domain.use_case.interactor.NoteListInteractor;

@Module
public interface DomainModule {

    @Binds
    IUseCase provideNoteListInteractor(NoteListInteractor noteListInteractor);
}
