package ua.igornovykov.todolist.ui.activity.login;

import android.text.TextUtils;
import android.util.Log;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ua.igornovykov.todolist.utils.RxLiveData;

public class LoginActivityViewModel extends ViewModel {
    private ILogin login;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private MutableLiveData<String> email = new MutableLiveData<>();
    private MutableLiveData<String> password = new MutableLiveData<>();
    private MutableLiveData<Boolean> isValidated = new MutableLiveData<>();

    private MutableLiveData<Boolean> isSuccessfulAuthentication = new MutableLiveData<>();

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.clear();
    }

    @Inject
    public LoginActivityViewModel(ILogin login) {
        this.login = login;

        email.setValue("");
        password.setValue("");
        isValidated.setValue(false);

        compositeDisposable.add(isAllFieldsValid()
                .subscribe(isValidated::postValue));
    }

    public void registration() {
        compositeDisposable.add(
                login.createUserWithEmailAndPassword(email.getValue(), password.getValue())
                        .subscribeOn(Schedulers.io())
                        .subscribe(isSuccessfulAuthentication::postValue, this::logger));
    }

    public void signing() {
        compositeDisposable.add(
                login.signInWithEmailAndPassword(email.getValue(), password.getValue())
                        .subscribeOn(Schedulers.io())
                        .subscribe(isSuccessfulAuthentication::postValue, this::logger));
    }

    private Observable<Boolean> isAllFieldsValid() {
        return Observable.combineLatest(
                RxLiveData.toObservable(email)
                        .observeOn(Schedulers.io()),
                RxLiveData.toObservable(password)
                        .observeOn(Schedulers.io()),
                (e, p) -> (!TextUtils.isEmpty(e) && !TextUtils.isEmpty(p)));
    }

    boolean isCurrentUser() {
        return login.isCurrentUser();
    }

    public MutableLiveData<String> getEmail() {
        return email;
    }

    public MutableLiveData<String> getPassword() {
        return password;
    }

    public LiveData<Boolean> getIsValidated() {
        return isValidated;
    }

    public LiveData<Boolean> getIsSuccessfulAuthentication() {
        return isSuccessfulAuthentication;
    }

    private void logger(Throwable throwable) {
        Log.e("ToDoList", throwable.getMessage());
    }
}
