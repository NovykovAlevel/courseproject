package ua.igornovykov.todolist.ui.adapter;

public interface NoteAdapterClickListener {
    void onClicked(long noteId);
}
