package ua.igornovykov.todolist.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

public class NotesInformationFragment extends Fragment implements View.OnClickListener {
    private OnHideClickListener listener;
    private FragmentNotesInfoBinding binding;

    public static NotesInformationFragment getInstance() {
        return new NotesInformationFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnHideClickListener) {
            listener = (OnHideClickListener) context;
        } else {
            throw new ClassCastException("Implement OnHideClickListener");
        }
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnHideClickListener) {
            listener = (OnHideClickListener) activity;
        } else {
            throw new ClassCastException("Implement OnHideClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_notes_info,
                container,
                false);

        binding.setLifecycleOwner(this);

        binding.hideInfo.setOnClickListener(this);
        return binding.getRoot();
    }

    public void setNotesInfo(NotesInfo info) {
        binding.setInfo(info);
    }

    @Override
    public void onClick(View v) {
        listener.onHideClick();
    }

    public interface OnHideClickListener {
        void onHideClick();
    }


    @Override
    public void onClick(View v) {

    }
}
