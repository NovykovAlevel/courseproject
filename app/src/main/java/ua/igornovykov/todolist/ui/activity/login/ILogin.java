package ua.igornovykov.todolist.ui.activity.login;

import io.reactivex.Single;

public interface ILogin {
    Single<Boolean> signInWithEmailAndPassword(String email, String password);

    Single<Boolean> createUserWithEmailAndPassword(String email, String password);

    boolean isCurrentUser();
}
