package ua.igornovykov.todolist.ui.activity.note_editor;

import android.text.TextUtils;
import android.util.Log;

import java.util.Date;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.todolist.ui.activity.Event;

public class NoteEditViewModel extends ViewModel {
    private final String EMPTY_FIELDS_MESSAGE = "Fill empty fields";
    private final String SAVE_ERROR_MESSAGE = "Save error";

    private String noteId;
    private IUseCase useCase;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public MutableLiveData<String> noteTitle = new MutableLiveData<>();
    public MutableLiveData<String> noteDescription = new MutableLiveData<>();
    private MutableLiveData<String> notePriority = new MutableLiveData<>();
    private MutableLiveData<Date> dateOfExpiration = new MutableLiveData<>();

    private MutableLiveData<Event<String>> message = new MutableLiveData<>();
    private MutableLiveData<Boolean> isSuccessfulUpdate = new MutableLiveData<>();

    @Inject
    public NoteEditViewModel(IUseCase useCase, String noteId) {
        this.useCase = useCase;
        this.noteId = noteId;

        noteTitle.setValue("");
        noteDescription.setValue("");
        notePriority.setValue("");
        getNote(noteId);
    }

    void getNote(String noteId) {
        if (!TextUtils.isEmpty(noteId)) {
            compositeDisposable.add(useCase.getNote(noteId)
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::fillingFields, this::reportError));
        }
    }

    private void fillingFields(Note note) {
        dateOfExpiration.postValue(note.getDateOfExpiration());
        noteTitle.postValue(note.getNoteTitle());
        noteDescription.postValue(note.getNoteDescription());
        notePriority.postValue(note.getNotePriority().name());
    }

    private void reportError(Throwable throwable) {
        logger(throwable);
        message.postValue(new Event<>(SAVE_ERROR_MESSAGE));
    }

    private void logger(Throwable throwable) {
        Log.e("ALevel", "logger: " + throwable.getMessage());
    }

    public void selectedPriority(String selectedPriority) {
        notePriority.setValue(selectedPriority);
    }

    private Note getNoteFromFields() {
        return new Note(noteId,
                noteTitle.getValue(),
                noteDescription.getValue(),
                Note.NotePriority.valueOf(notePriority.getValue()),
                new Date(),
                dateOfExpiration.getValue());
    }

    private boolean isEmptyFields() {
        return TextUtils.isEmpty(noteTitle.getValue())
                || TextUtils.isEmpty(noteDescription.getValue())
                || TextUtils.isEmpty(notePriority.getValue())
                || dateOfExpiration.getValue() == null;
    }

    private void save(Note note) {
        compositeDisposable.add(useCase.saveNote(note)
                .subscribeOn(Schedulers.io())
                .subscribe(() -> isSuccessfulUpdate.postValue(true), this::reportError));

    }

    public void setDate(Date date) {
        dateOfExpiration.setValue(date);
    }

    public LiveData<Date> getExpirationDate() {
        return dateOfExpiration;
    }

    public LiveData<String> getPriority() {
        return notePriority;
    }

    LiveData<Event<String>> getShowMessage() {
        return message;
    }

    LiveData<Boolean> getIsSuccessfulUpdate() {
        return isSuccessfulUpdate;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }
}
