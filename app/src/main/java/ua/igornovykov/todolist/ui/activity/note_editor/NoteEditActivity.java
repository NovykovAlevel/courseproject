package ua.igornovykov.todolist.ui.activity.note_editor;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.R;
import ua.igornovykov.todolist.databinding.ActivityNoteEditBinding;
import ua.igornovykov.todolist.ui.activity.Event;
import ua.igornovykov.todolist.ui.activity.note_list.NoteListActivity;

public class NoteEditActivity extends DaggerAppCompatActivity implements DateDialogFragment.OnSaveClickListener {
    private static final String INTENT_KEY_NOTE = "INTENT_KEY_NOTE";

    private NoteEditViewModel vm;

    @Inject
    NoteEditViewModelFactory viewModelFactory;

    public static Intent getIntent(Context context, String noteId) {
        Intent intent = new Intent(context, NoteEditActivity.class);
        intent.putExtra(INTENT_KEY_NOTE, noteId);
        return intent;
    }

    public String getNoteId() {
        return getIntent().getStringExtra(INTENT_KEY_NOTE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        vm = ViewModelProviders
                .of(this, viewModelFactory)
                .get(NoteEditViewModel.class);

        ActivityNoteEditBinding editorBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_note_edit);
        editorBinding.setLifecycleOwner(this);
        editorBinding.setVm(vm);
        editorBinding.showDateCreationDialog.setOnClickListener(this::showDateCreationDialog);

        vm.getShowMessage().observe(this, this::showMessage);
        vm.getIsSuccessfulUpdate().observe(this, this::editNote);
    }

    private void showMessage(Event<String> event) {
        if (!event.isHandled()) {
            event.setHandled(true);
            Toast.makeText(this, event.getData(), Toast.LENGTH_SHORT).show();
        }
    }

    public void showDateCreationDialog(View view) {
        DateDialogFragment.getInstance()
                .show(getSupportFragmentManager(), "DateDialogFragment");
    }

    private void editNote(Boolean isSuccess) {

        if (isSuccess) {
            Intent intent = NoteListActivity.getIntent(this);
            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
