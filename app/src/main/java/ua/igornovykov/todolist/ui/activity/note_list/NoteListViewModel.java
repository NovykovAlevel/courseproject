package ua.igornovykov.todolist.ui.activity.note_list;

import android.util.Log;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.domain.use_case.IUseCase;
import ua.igornovykov.todolist.entities.NotesStatisticInfo;
import ua.igornovykov.todolist.ui.activity.Event;
import ua.igornovykov.todolist.utils.InformationCreator;
import ua.igornovykov.todolist.utils.RxLiveData;
import ua.igornovykov.todolist.utils.SortingMethodFactory;

public class NoteListViewModel extends ViewModel {
    private IUseCase useCase;
    private int choosedMenuItemId;
    private InformationCreator informationCreator;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private MutableLiveData<List<Note>> allNotes = new MutableLiveData<>();
    private MutableLiveData<Comparator<Note>> sortingMethod = new MutableLiveData<>();
    private MutableLiveData<List<Note>> sortedListNotes = new MutableLiveData<>();

    private MutableLiveData<Boolean> isShowListNotes = new MutableLiveData<>();
    private MutableLiveData<Boolean> isShowProgress = new MutableLiveData<>();

    private MutableLiveData<NotesStatisticInfo> notesInfo = new MutableLiveData<>();
    private MutableLiveData<Boolean> isShowNotesInfo = new MutableLiveData<>();

    @Inject
    public NoteListViewModel(IUseCase useCase, InformationCreator informationCreator) {
        this.useCase = useCase;
        this.informationCreator = informationCreator;

        sortingMethod.setValue(SortingMethodFactory.sortByPriority());
        getAllNotes();
        compositeDisposable.add(getSortedListNotesObservable()
                .subscribe(sortedListNotes::postValue));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        compositeDisposable.dispose();
    }

    void getAllNotes() {
        compositeDisposable.add(useCase.getNoteList()
                .doOnSubscribe(disposable -> showProgress(true))
                .doOnSuccess(list -> showProgress(false))
                .subscribeOn(Schedulers.io())
                .subscribe(allNotes::postValue));
    }

    void remove(Note note) {
        compositeDisposable.add(useCase.removeNote(note.getId())
                .subscribeOn(Schedulers.io())
                .subscribe(this::getAllNotes));
    }

    void setSortingMethod(Comparator<Note> sortingMethod) {
        this.sortingMethod.setValue(sortingMethod);
    }

    void hideNotesInfo() {
        isShowNotesInfo.setValue(false);
    }

    void showNotesInfo() {
        isShowNotesInfo.setValue(true);
        List<Note> notes = allNotes.getValue();

        if (notes != null) {
            compositeDisposable.add(getNoteInfo(notes)
                    .subscribeOn(Schedulers.computation())
                    .subscribe(notesInfo::postValue, this::logger));
        }
    }

    int getChoosedMenuItemId() {
        return choosedMenuItemId;
    }

    void setChooseedMenuItemId(int choosedMenuItemId) {
        this.choosedMenuItemId = choosedMenuItemId;
    }

    private void showProgress(boolean showProgress){
        isShowProgress.postValue(showProgress);
        isShowListNotes.postValue(!showProgress);
    }

    private Single<NotesStatisticInfo> getNoteInfo(List<Note> notes) {
        return Single.fromCallable(() -> informationCreator.create(notes));
    }

    private void sortNotesList() {
        compositeDisposable.add(getSortedListNotesObservable()
                .subscribe(sortedListNotes::postValue));
    }

    private Observable<List<Note>> getSortedListNotesObservable() {
        return Observable.combineLatest(
                RxLiveData.toObservable(allNotes)
                        .observeOn(Schedulers.computation()),
                RxLiveData.toObservable(sortingMethod)
                        .observeOn(Schedulers.computation()),
                (notesList, noteComparator) -> {
                    if (notesList != null) {
                        Collections.sort(notesList, noteComparator);
                    }
                    return notesList;
                });
    }

    private void logger(Throwable throwable) {
        Log.e("ToDoList", throwable.getMessage());
    }

    public LiveData<List<Note>> getSortedListNotes() {
        return sortedListNotes;
    }

    public LiveData<Boolean> getIsShowProgress() {
        return isShowProgress;
    }

    public LiveData<Boolean> getIsShowListNotes() {
        return isShowListNotes;
    }

    public LiveData<Boolean> getIsShowNotesInfo() {
        return isShowNotesInfo;
    }

    public LiveData<NotesStatisticInfo> getNotesInfo() {
        return notesInfo;
    }
}
