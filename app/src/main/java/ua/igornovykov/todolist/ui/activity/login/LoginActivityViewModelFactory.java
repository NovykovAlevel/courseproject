package ua.igornovykov.todolist.ui.activity.login;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class LoginActivityViewModelFactory implements ViewModelProvider.Factory {
    private LoginActivityViewModel vm;

    @Inject
    public LoginActivityViewModelFactory(LoginActivityViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) vm;
    }
}
