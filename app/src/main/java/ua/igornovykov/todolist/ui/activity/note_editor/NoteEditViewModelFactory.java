package ua.igornovykov.todolist.ui.activity.note_editor;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class NoteEditViewModelFactory implements ViewModelProvider.Factory {
    private NoteEditViewModel vm;

    @Inject
    NoteEditViewModelFactory(NoteEditViewModel vm) {
        this.vm = vm;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        //noinspection unchecked
        return (T) vm;
    }
}
