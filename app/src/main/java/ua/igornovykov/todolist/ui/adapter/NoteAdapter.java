package ua.igornovykov.todolist.ui.adapter;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.R;
import ua.igornovykov.todolist.ui.widgets.MyCustomView;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteViewHolder> {
    private List<Note> notes;
    private OnRemoveNoteListener removeListener;
    private NoteAdapterClickListener noteAdapterClickListener;

    @Inject
    public NoteAdapter() {
        notes = Collections.emptyList();
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int itemType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View itemView = layoutInflater.inflate(R.layout.note_item, viewGroup, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder noteViewHolder, int position) {
        noteViewHolder.bind(getNoteByPosition(position));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public void setData(List<Note> notes) {
        this.notes = notes;
        Collections.sort(this.notes);
        notifyDataSetChanged();
    }

    private Note getNoteByPosition(int position) {
        return notes.get(position);
    }

    public void setRemoveListener(OnRemoveNoteListener removeListener) {
        this.removeListener = removeListener;
    }

    public void setNoteAdapterClickListener(NoteAdapterClickListener noteAdapterClickListener) {
        this.noteAdapterClickListener = noteAdapterClickListener;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView noteTitleTextView;
        private AppCompatTextView noteDescriptionTextView;
        private AppCompatImageView deleteNoteImageView;
        private MyCustomView notePriorityImageView;

        NoteViewHolder(@NonNull View itemView) {
            super(itemView);

            noteTitleTextView = itemView.findViewById(R.id.noteTitleTextView);
            noteDescriptionTextView = itemView.findViewById(R.id.noteDescriptionTextView);
            notePriorityImageView = itemView.findViewById(R.id.notePriorityImageView1);
            deleteNoteImageView = itemView.findViewById(R.id.deleteNoteImageView);
        }

        void bind(final Note note) {
            deleteNoteImageView.setOnClickListener(v -> onRemoveClick(note));
            itemView.setOnClickListener(v -> onItemClick(note));

            noteTitleTextView.setText(note.getNoteTitle());
            noteDescriptionTextView.setText(note.getNoteDescription());
            switch (note.getNotePriority()) {
                case LOW:
                    notePriorityImageView.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
                    notePriorityImageView.setImageResource(R.drawable.ic_priority_low);
                    break;
                case MIDDLE:
                    notePriorityImageView.setBackgroundTintList(ColorStateList.valueOf(Color.YELLOW));
                    notePriorityImageView.setImageResource(R.drawable.ic_priority_middle);
                    break;
                case HIGH:
                    notePriorityImageView.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
                    notePriorityImageView.setImageResource(R.drawable.ic_priority_high);
                    break;
            }

        }
        private void onItemClick(Note note) {
            if (noteAdapterClickListener != null) {
                noteAdapterClickListener.onClicked(note.getId());
            }
        }

        private void onRemoveClick(Note note) {
            if (removeListener != null) {
                removeListener.onRemoveNote(note);
            }
        }
    }

    public interface OnRemoveNoteListener {
        void onRemoveNote(Note note);
    }
}
