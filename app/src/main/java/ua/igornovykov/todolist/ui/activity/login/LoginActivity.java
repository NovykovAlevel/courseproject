package ua.igornovykov.todolist.ui.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import javax.inject.Inject;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import dagger.android.support.DaggerAppCompatActivity;
import ua.igornovykov.todolist.R;
import ua.igornovykov.todolist.databinding.ActivityLoginBinding;
import ua.igornovykov.todolist.ui.activity.note_list.NoteListActivity;

public class LoginActivity extends DaggerAppCompatActivity {
    @Inject
    LoginActivityViewModelFactory factory;
    private LoginActivityViewModel vm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vm = ViewModelProviders
                .of(this, factory)
                .get(LoginActivityViewModel.class);

        ActivityLoginBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setLifecycleOwner(this);
        binding.setVm(vm);

        observerData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (vm.isCurrentUser()) {
            updateUi(true);
        }
    }

    private void observerData() {
        vm.getIsSuccessfulAuthentication().observe(this, this::updateUi);
    }

    private void updateUi(Boolean isSuccess) {
        if (isSuccess != null && isSuccess) {
            Intent intent = new Intent(this, NoteListActivity.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Authentication failed!", Toast.LENGTH_SHORT).show();
        }
    }
}
