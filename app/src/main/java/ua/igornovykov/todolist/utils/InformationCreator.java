package ua.igornovykov.todolist.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import ua.igornovykov.domain.entity.Note;
import ua.igornovykov.todolist.entities.NotesStatisticInfo;

public class InformationCreator {
    private static final int defaultTrackingRangeExpiredNotesInDays = 5;
    private int countNotesWithHighPriority;
    private int countNotesWithMiddlePriority;
    private int countNotesWithLowPriority;
    private int countInvalidNotes;
    private int countInvalidInFutureNotes;
    private int countChangedOnMondayNotes;
    private int countChangedOnTuesdayNotes;
    private int countChangedOnWednesdayNotes;
    private int countChangedToThursdayNotes;
    private int countChangedToFridayNotes;
    private int countChangedToSaturdayNotes;
    private int countChangedToSundayNotes;

    @Inject
    public InformationCreator() {
    }

    private void clear() {
        countNotesWithHighPriority = 0;
        countNotesWithMiddlePriority = 0;
        countNotesWithLowPriority = 0;
        countInvalidNotes = 0;
        countInvalidInFutureNotes = 0;
        countChangedOnMondayNotes = 0;
        countChangedOnTuesdayNotes = 0;
        countChangedOnWednesdayNotes = 0;
        countChangedToThursdayNotes = 0;
        countChangedToFridayNotes = 0;
        countChangedToSaturdayNotes = 0;
        countChangedToSundayNotes = 0;
    }

    public NotesStatisticInfo create(List<Note> notes) {
        return create(notes, defaultTrackingRangeExpiredNotesInDays);
    }

    public NotesStatisticInfo create(List<Note> notes, int trackingRangeExpiredNotesInDays) {
        clear();
        for (Note note : notes) {
            gatheringInformationByPriorities(note);
            gatheringInformationByInvalid(note.getNoteStatus());
            gatheringInformationByInvalidInFuture(note.getDateOfExpiration(), trackingRangeExpiredNotesInDays);
            gatheringInformationByDayOfWeek(note.getDateOfCreation());
        }
        return createNotesInfo();
    }

    private void gatheringInformationByPriorities(Note note) {
        switch (note.getNotePriority()) {
            case HIGH:
                countNotesWithHighPriority++;
                break;
            case MIDDLE:
                countNotesWithMiddlePriority++;
                break;
            case LOW:
                countNotesWithLowPriority++;
        }
    }

    private void gatheringInformationByInvalid(Note.NoteStatus validity) {
        if (validity.equals(Note.NoteStatus.INVALID)) {
            countInvalidNotes++;
        }
    }

    private void gatheringInformationByInvalidInFuture(Date expirationDate, int trackingRangeExpiredNotesInDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, trackingRangeExpiredNotesInDays);
        if (calendar.getTime().after(expirationDate)) {
            countInvalidInFutureNotes++;
        }
    }

    private void gatheringInformationByDayOfWeek(Date creationDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationDate);

        switch (calendar.get(Calendar.DAY_OF_WEEK)) {
            case Calendar.MONDAY:
                countChangedOnMondayNotes++;
                break;
            case Calendar.TUESDAY:
                countChangedOnTuesdayNotes++;
                break;
            case Calendar.WEDNESDAY:
                countChangedOnWednesdayNotes++;
                break;
            case Calendar.THURSDAY:
                countChangedToThursdayNotes++;
                break;
            case Calendar.FRIDAY:
                countChangedToFridayNotes++;
                break;
            case Calendar.SATURDAY:
                countChangedToSaturdayNotes++;
                break;
            case Calendar.SUNDAY:
                countChangedToSundayNotes++;
                break;
        }
    }

    private NotesStatisticInfo createNotesInfo() {
        return new NotesStatisticInfo.Builder()
                .countNotesWithHighPriority(countNotesWithHighPriority)
                .countNotesWithMiddlePriority(countNotesWithMiddlePriority)
                .countNotesWithLowPriority(countNotesWithLowPriority)
                .countInvalidNotes(countInvalidNotes)
                .countInvalidInFutureNotes(countInvalidInFutureNotes)
                .countChangedOnMondayNotes(countChangedOnMondayNotes)
                .countChangedOnTuesdayNotes(countChangedOnTuesdayNotes)
                .countChangedOnWednesdayNotes(countChangedOnWednesdayNotes)
                .countChangedToThursdayNotes(countChangedToThursdayNotes)
                .countChangedToFridayNotes(countChangedToFridayNotes)
                .countChangedToSaturdayNotes(countChangedToSaturdayNotes)
                .countChangedToSundayNotes(countChangedToSundayNotes)
                .build();
    }
}