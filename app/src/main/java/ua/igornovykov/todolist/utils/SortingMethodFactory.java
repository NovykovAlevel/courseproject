package ua.igornovykov.todolist.utils;

import java.util.Comparator;

import ua.igornovykov.domain.entity.Note;

public final class SortingMethodFactory {

    private SortingMethodFactory() {
    }

    public static Comparator<Note> sortByTitle() {
        return (not1, not2) -> not1.getNoteTitle().compareTo(not2.getNoteTitle());
    }

    public static Comparator<Note> sortByPriority() {
        return (note1, note2) -> note1.getNotePriority().compareTo(note2.getNotePriority());
    }

    public static Comparator<Note> sortByLastChange() {
        return (note1, note2) -> {
            if (note1.getDateOfCreation().before(note2.getDateOfCreation())) {
                return 1;
            } else if (note1.getDateOfCreation().after(note2.getDateOfCreation())) {
                return -1;
            }
            return 0;
        };
    }

    public static Comparator<Note> sortByEndTime() {
        return (note1, note2) -> note1.getDateOfExpiration().compareTo(note2.getDateOfExpiration());
    }
}
