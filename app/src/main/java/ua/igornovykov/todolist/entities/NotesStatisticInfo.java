package ua.igornovykov.todolist.entities;

public class NotesStatisticInfo {
    private int countNotesWithHighPriority;
    private int countNotesWithMiddlePriority;
    private int countNotesWithLowPriority;
    private int countInvalidNotes;
    private int countInvalidInFutureNotes;
    private int countChangedOnMondayNotes;
    private int countChangedOnTuesdayNotes;
    private int countChangedOnWednesdayNotes;
    private int countChangedToThursdayNotes;
    private int countChangedToFridayNotes;
    private int countChangedToSaturdayNotes;
    private int countChangedToSundayNotes;

    private NotesStatisticInfo() {
    }

    public static class Builder {
        private NotesStatisticInfo notesStatisticInfo;

        public Builder() {
            notesStatisticInfo = new NotesStatisticInfo();
        }

        public Builder countNotesWithHighPriority(int countNotesWithHighPriority) {
            notesStatisticInfo.countNotesWithHighPriority = countNotesWithHighPriority;
            return this;
        }

        public Builder countNotesWithMiddlePriority(int countNotesWithMiddlePriority) {
            notesStatisticInfo.countNotesWithMiddlePriority = countNotesWithMiddlePriority;
            return this;
        }

        public Builder countNotesWithLowPriority(int countNotesWithLowPriority) {
            notesStatisticInfo.countNotesWithLowPriority = countNotesWithLowPriority;
            return this;
        }

        public Builder countInvalidNotes(int countInvalidNotes) {
            notesStatisticInfo.countInvalidNotes = countInvalidNotes;
            return this;
        }

        public Builder countInvalidInFutureNotes(int countInvalidInFutureNotes) {
            notesStatisticInfo.countInvalidInFutureNotes = countInvalidInFutureNotes;
            return this;
        }

        public Builder countChangedOnMondayNotes(int countChangedOnMondayNotes) {
            notesStatisticInfo.countChangedOnMondayNotes = countChangedOnMondayNotes;
            return this;
        }

        public Builder countChangedOnTuesdayNotes(int countChangedOnTuesdayNotes) {
            notesStatisticInfo.countChangedOnTuesdayNotes = countChangedOnTuesdayNotes;
            return this;
        }

        public Builder countChangedOnWednesdayNotes(int countChangedOnWednesdayNotes) {
            notesStatisticInfo.countChangedOnWednesdayNotes = countChangedOnWednesdayNotes;
            return this;
        }

        public Builder countChangedToThursdayNotes(int countChangedToThursdayNotes) {
            notesStatisticInfo.countChangedToThursdayNotes = countChangedToThursdayNotes;
            return this;
        }

        public Builder countChangedToFridayNotes(int countChangedToFridayNotes) {
            notesStatisticInfo.countChangedToFridayNotes = countChangedToFridayNotes;
            return this;
        }

        public Builder countChangedToSaturdayNotes(int countChangedToSaturdayNotes) {
            notesStatisticInfo.countChangedToSaturdayNotes = countChangedToSaturdayNotes;
            return this;
        }

        public Builder countChangedToSundayNotes(int countChangedToSundayNotes) {
            notesStatisticInfo.countChangedToSundayNotes = countChangedToSundayNotes;
            return this;
        }

        public NotesStatisticInfo build(){
            return notesStatisticInfo;
        }
    }

    public int getCountNotesWithHighPriority() {
        return countNotesWithHighPriority;
    }

    public int getCountNotesWithMiddlePriority() {
        return countNotesWithMiddlePriority;
    }

    public int getCountNotesWithLowPriority() {
        return countNotesWithLowPriority;
    }

    public int getCountInvalidNotes() {
        return countInvalidNotes;
    }

    public int getCountInvalidInFutureNotes() {
        return countInvalidInFutureNotes;
    }

    public int getCountChangedOnMondayNotes() {
        return countChangedOnMondayNotes;
    }

    public int getCountChangedOnTuesdayNotes() {
        return countChangedOnTuesdayNotes;
    }

    public int getCountChangedOnWednesdayNotes() {
        return countChangedOnWednesdayNotes;
    }

    public int getCountChangedToThursdayNotes() {
        return countChangedToThursdayNotes;
    }

    public int getCountChangedToFridayNotes() {
        return countChangedToFridayNotes;
    }

    public int getCountChangedToSaturdayNotes() {
        return countChangedToSaturdayNotes;
    }

    public int getCountChangedToSundayNotes() {
        return countChangedToSundayNotes;
    }
}
